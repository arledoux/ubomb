package fr.ubx.poo.ubomb.game;

public record Position (int x, int y) {

    public Position(Position position) {
        this(position.x, position.y);
    }


    public boolean equals (Position position){
        if ((this.x == position.x) && (this.y == position.y)){
            return true;
        }
        else {
            return false;
        }
    }

}
