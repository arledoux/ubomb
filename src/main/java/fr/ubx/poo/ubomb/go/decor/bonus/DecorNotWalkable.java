package fr.ubx.poo.ubomb.go.decor.bonus;

import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.character.*;
import fr.ubx.poo.ubomb.go.decor.Decor;

public class DecorNotWalkable extends Decor {

    public DecorNotWalkable(Game game, Position position) {
        super(game, position);
    }

    public DecorNotWalkable(Position position) {
        super(position);
    }

    public boolean walkableBy(Player player) { return false; }

    public boolean walkableBy(Monster monster) { return false; }
}
