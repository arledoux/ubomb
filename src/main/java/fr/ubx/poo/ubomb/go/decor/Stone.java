/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.go.decor;

import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.Walkable;
import fr.ubx.poo.ubomb.go.decor.bonus.DecorNotWalkable;

public class Stone extends DecorNotWalkable {
    public Stone(Position position) {
        super(position);
    }


}
