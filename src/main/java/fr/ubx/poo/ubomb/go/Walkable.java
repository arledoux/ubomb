package fr.ubx.poo.ubomb.go;

import fr.ubx.poo.ubomb.go.character.*;

public interface Walkable {
    default boolean walkableBy(Player player) { return true; }
    default boolean walkableBy(Monster monster) { return true; }
}
