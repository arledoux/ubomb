/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.go.decor;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.Movable;
import fr.ubx.poo.ubomb.go.character.*;
import fr.ubx.poo.ubomb.game.*;
import fr.ubx.poo.ubomb.launcher.*;
import fr.ubx.poo.ubomb.go.Walkable;
import fr.ubx.poo.ubomb.go.decor.bonus.Bonus;
import fr.ubx.poo.ubomb.go.decor.bonus.DecorNotWalkable;

import javax.swing.text.html.parser.Entity;

public class Box extends Decor implements Movable {
    public Box(Position position) {
        super(position);
    }

    public boolean canMove(Direction direction){
        return false;
    }

    public boolean canMove(Direction direction, Game game){
        Position nextPos = direction.nextPosition(getPosition());
        GameObject next = game.grid().get(nextPos);
        if (next == null)
            if(game.grid().inside(nextPos))
                return true;
        return false;
    }
    public void doMove(Direction direction){

    }
    public void doMove(Direction direction, Game game){
        // This method is called only if the move is possible, do not check again

        Position nextPos = direction.nextPosition(getPosition());
       // GameObject next = game.grid().get(nextPos);
        this.setPosition(nextPos);
        game.grid().set(this.getPosition(),this);
        this.setModified(true);



        //game.grid().set(this.getPosition(),);


    }
}
