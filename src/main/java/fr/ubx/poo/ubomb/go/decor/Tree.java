/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ubomb.go.decor;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.decor.bonus.DecorNotWalkable;

public class Tree extends DecorNotWalkable {
    public Tree(Position position) {
        super(position);
    }
}
