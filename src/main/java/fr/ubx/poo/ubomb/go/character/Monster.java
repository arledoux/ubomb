package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.Movable;
import fr.ubx.poo.ubomb.engine.*;
import fr.ubx.poo.ubomb.go.decor.Box;
import fr.ubx.poo.ubomb.go.decor.bonus.Bonus;

public class Monster extends GameObject implements Movable {

    private Direction direction;
    private boolean moveRequested = false;
    private int lives;
    private Timer time;

    public Monster(Game game, Position position) {
        super(game, position);
        this.lives = 1;
        this.direction = Direction.random();
        time = new Timer((8000/(game.configuration().monsterVelocity())));
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean canMove(Direction direction){
        Position nextPos = direction.nextPosition(getPosition());
        GameObject next = game.grid().get(nextPos);
        if(next instanceof Box) {
            return false;
        }
        if (next == null || next.walkableBy(this))
            if(game.grid().inside(nextPos))
                return true;


        return false;
    }

    public void doMove(Direction direction){
        Position nextPos = direction.nextPosition(getPosition());
        setPosition(nextPos);
    }

    public void update(long now) {
        time.start();
        time.update(now);
        if (!time.isRunning()){
            int i = 0;
            while(i<4){
                this.direction = Direction.random();
                if (canMove(this.direction)){
                    doMove(this.direction);
                    i=4;
                }
                i++;
            }
        }
    }

}
