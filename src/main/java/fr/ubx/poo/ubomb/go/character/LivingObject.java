package fr.ubx.poo.ubomb.go.character;

import fr.ubx.poo.ubomb.game.Direction;
import fr.ubx.poo.ubomb.game.Game;
import fr.ubx.poo.ubomb.game.Position;
import fr.ubx.poo.ubomb.go.GameObject;
import fr.ubx.poo.ubomb.go.Movable;
import fr.ubx.poo.ubomb.go.TakeVisitor;

public abstract class LivingObject extends GameObject implements Movable, TakeVisitor {
    private Direction direction;
    private int lives;

    public LivingObject(Game game, Position position, int live) {
        super(game, position);
        this.direction = Direction.random();
        this.lives = live;
    }

    public boolean canMove(Direction direction){
        Position nextPos = direction.nextPosition(getPosition());
        GameObject next = game.grid().get(nextPos);
        if (next == null)
            if(game.grid().inside(nextPos))
                return true;


        return false;
    }

    public void doMove(Direction direction){

    }

    public void damaged() {
        lives --;
    }
}
